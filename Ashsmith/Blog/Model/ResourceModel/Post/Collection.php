<?php namespace Ashsmith\Blog\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ashsmith\Blog\Model\Post', 'Ashsmith\Blog\Model\ResourceModel\Post');
    }

    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => $this->getMainTable()]);
        $this->addFilterToMap('created_at', 'main_table.created_at');
        return $this;
    }
}
