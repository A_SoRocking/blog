<?php

declare(strict_types=1);

namespace Ashsmith\Blog\Model;

use Ashsmith\Blog\Api\PostRepositoryInterface;
use Ashsmith\Blog\Api\Data\PostInterface;
use Ashsmith\Blog\Api\Data\PostSearchResultsInterfaceFactory;
use Ashsmith\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Ashsmith\Blog\Model\ResourceModel\Post;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Ashsmith\Blog\Api\Data\PostSearchResultsInterface;

class PostRepository implements PostRepositoryInterface
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var PostCollectionFactory
     */
    private $postCollectionFactory;

    /**
     * @var PostSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ResourceModel\Post
     */
    private $postResource;

    /**
     * @param PostFactory $postFactory
     * @param PostCollectionFactory $postCollectionFactory
     * @param PostSearchResultsInterfaceFactory $postSearchResultsInterfaceFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ResourceModel\Post $postResource
     */
    public function __construct(
        PostFactory $postFactory,
        PostCollectionFactory $postCollectionFactory,
        PostSearchResultsInterfaceFactory $postSearchResultsInterfaceFactory,
        CollectionProcessorInterface $collectionProcessor,
        post $postResource
    ) {
        $this->postFactory = $postFactory;
        $this->postCollectionFactory = $postCollectionFactory;
        $this->searchResultFactory = $postSearchResultsInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->postResource = $postResource;
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): PostInterface
    {
        $post = $this->postFactory->create();
        $this->postResource->load($post, $id);
        if (!$post->getId()) {
            throw new NoSuchEntityException(__('Unable to find Post with ID "%1"', $id));
        }
        return $post;
    }

    /**
     * @inheritDoc
     */
    public function save(PostInterface $post): PostInterface
    {
        $this->postResource->save($post);
        return $post;
    }

    /**
     * @inheritDoc
     */
    public function delete(PostInterface $post): void
    {
        $this->postResource->delete($post);
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): PostSearchResultsInterface
    {
        $collection = $this->postCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, ($collection));

        /** @var $searchResults */
        $searchResults = $this->searchResultFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }
}
