<?php

declare(strict_types=1);

namespace Ashsmith\Blog\Api\Data;

interface PostInterface
{
    const POST_ID = 'post_id';
    const TITLE = 'title';
    const CREATED_AT = 'created_at';
    const IMAGE = 'image';
    const DESCRIPTION = 'description';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Set ID
     *
     * @param int $id
     * @return PostInterface
     */
    public function setId(int $id): PostInterface;

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Set title
     *
     * @param string $title
     * @return PostInterface
     */
    public function setTitle(string $title): PostInterface;

    /**
     * Get created at
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return PostInterface
     */
    public function setCreatedAt(string $createdAt): PostInterface;

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * Set image
     *
     * @param $image
     * @return PostInterface
     */
    public function setImage(string $image): PostInterface;

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * Set description
     *
     * @param string $description
     * @return PostInterface
     */
    public function setDescription(string $description): PostInterface;
}
