<?php

declare(strict_types=1);

namespace Ashsmith\Blog\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Ashsmith\Blog\Api\Data\PostInterface;
use Ashsmith\Blog\Api\Data\PostSearchResultsInterface;

interface PostRepositoryInterface
{
    /**
     * Load Post data by given ID
     *
     * @param int $id
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById(int $id): PostInterface;

    /**
     * Save Post
     *
     * @param \Ashsmith\Blog\Api\Data\PostInterface $post
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function save(PostInterface $post): PostInterface;

    /**
     * Delete Post
     *
     * @param \Ashsmith\Blog\Api\Data\PostInterface $post
     * @return void
     */
    public function delete(PostInterface $post): void;

    /**
     * Load Post data collection by given search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Ashsmith\Blog\Api\Data\PostSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): postSearchResultsInterface;
}
